[SIESTA](https://siesta-project.org) is a software for efficient electronic structure calculations and ab initio molecular dynamics simulations of molecules and solids.

This project contains the source for the top-level documentation for the SIESTA project suite of programs, utilities, and data.

